<?php

/**
 * @file
 *  Contains the code to provide the [ecpos] Territory Manager view html to a user. This view converts
 *  A Territory number to a set of zip codes and uses these to generate an accurate view of the territory
 *  history as it is configured today. It provides the POS/DS data of past transactions based on the current
 *  zip mapping.
 */

/**
 * Function to show the contextual managerview page per processing of
 * the URL, user, and thier roles. This view processes urls that contain
 * the $tid query item, auto redirect users with a single associate $tid, 
 * or proivdes the user the ability to select a $tid if thier account is
 * associated with multiple TM#s
 *
 * @return string
 *  Html markup for Drupal rendering
 */
function ecpos_managerview_view() {
   // Get territory id from the url
  $tid = isset($_GET['tid']) ? check_plain((int)$_GET['tid']) : null;
  // If the url provides a proper $tid, build and add the page markup
  if(!is_null($tid) && !in_array($tid, array(0, ''))) {
    $markup = ecpos_managerview_page($tid);
  }
  // If no proper tid is provided, determine users applicable territories
  else {
    // Initialize variables
    global $user;
    $uid = $user->uid;
    // Territory manager
    if(isset($user->roles[7])) {
      $tms = array();
      // Load full user
      $user = user_load($uid);
      // Logic for uses with more than one tm
      if(count($user->field_emuge_tmn['und']) > 1) {
        // Add the users tms to the $tms array
        foreach($user->field_emuge_tmn['und'] as $key => $val) {
          array_unshift($tms, $val['value']);
        }
      }
      // Logic for uses with just one tm
      elseif(count($user->field_emuge_tmn['und']) == 1) {
        $tid = $user->field_emuge_tmn['und'][0]['value']; 
      }
    }
    // Admin, staff, or accounting
    elseif(isset($user->roles[3]) || isset($user->roles[6]) || isset($user->roles[11])) {
      // Initialize variables
      $posTable = 'emugecom_pos_transactions';
      $tms      = array();
      // Build database query of available transaction territory ids
      $query = db_select($posTable, 't')->fields('t', array('tid'));
      $query->distinct();
      // Execute the $query
      try {
        $results = $query->execute()->fetchAll();
      } catch(PDOException $e) {
        // @TODO turn this into watchdog logs
        $results = false;
      }
      // Add the queried tms to the $tms array
      foreach($results as $key => $val) {
        if($val->tid > 0) {
          array_push($tms, $val->tid);
        }
      }
    }
    // If the $tid variable is not null, 0, or '' we redirect to the results page
    if(!is_null($tid) && !in_array($tid, array('0', ''))) {
      drupal_goto('/pos/managerview', array('query' => array('tid' => $tid)));
    }
    // Else if we have set the $tms array, meaning multiple potnential tms,
    // We create and render the select form in the page markup
    elseif(!is_null($tms)) {
      $form   = drupal_get_form('ecpos_managerview_tid_select_form', $tms);
      $markup = drupal_render($form);
    }
    // If all else fails, provide gracefull landing
    else {
      $markup = 'Thre are no results, please contact your site administrator if you believe you have reached this page in error.';
    }
  }
  // Retrun the proper $page array and its markup
  return $markup;
}
 
 /**
  * Function to generate html for the Manager View for the
  * POS/Drop Shit Application.
  *
  * @param string
  *   The territory id to show inoformation about
  *
  * @return string
  *   HTML markup showing the data tables for a territory 
  */
function ecpos_managerview_page($tid) {
  // Set the title of the page contextually
  drupal_set_title("Territory #{$tid} POS/DropShip Reporting"); 
  // Initialize the functions variables
  $markup = '<p>This report displays the comparable historical sales data of your territory as it exists today. 
            It provides Dropship information and POS data from participating distributors. 
            If your sales territory has changed in the past 12 months, this data is updated to reflect the change.</p>';
  $zips   = array();
  $tables = array();
 
  // This table holds the relationships between a territory id and its 
  // corresponding zip codes
  $zipTable   = 'emugecom_zip_state_territory_map';
  // This table holds all of the transactions used to generate the reprorts
  $transTable = 'emugecom_pos_transactions';
  // This table holds the distributor/drop ship names and weight
  $distTable  = 'emugecom_pos_distributor_info';
  // The results table header row cell names
  $header     = array('', 'YTD', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Total');
  // Prototype for the Total table array 
  $table = array(
    'label'   => 'Total POS/Dropship',
    'id'      => 0,
    'classes' => '',
    'row1'    => '2015',
    'row2'    => '2016'
  );
  // Load the depecdency [ecreports] to generate table skeletons of tables
 #@TODO remove this link and test
  module_load_include('inc', 'ecreports', 'ecreports.salesview');
  // Call the add_table function to create these tables
 #$newTable = ecreports_view_salesview_add_table($table, $table['row1'], $table['row2']);
  $newTable = _ecpos_add_table($table, $table['row1'], $table['row2']);
  // Call the get sales query function
  $records  = _ecpos_get_pos_sales($tid);
  // Work with resulting records from the zips/years search 
  if($records) {
    // Initalize path and territory variables
    $path = check_plain($_GET['q']);
    $tid  = check_plain($_GET['tid']);
    // Prepate the pdf and print links markup
    $links = 
        '<div class="ec-tpr-links hide-links">
        <a href="/printpdf/' . $path . '?tid=' . $tid .'" style="float:right;" class="boxed-button ec-tpr-link"  onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">pdf</a>
        <a href="/print/' . $path . '?tid=' . $tid .'" style="float:right;"  class="boxed-button ec-tpr-link" onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">print</a>
        </div>';
    $markup .= $links;
    // Create the Totals table with the $table array
    $tables[0] = _ecpos_add_table($table, $table['row1'], $table['row2']);
    // Add the records to the $tables array
    if($records) {
     foreach($records as $r) {
      // Initialize a non-existant table
      if(!isset($tables[$r->pdid])) {
        $type             = ($r->pdid == 5) ? '' : ' POS';
        $table['label']   = $r->name . $type;
        $table['id']      = $r->pdid;
        $tables[$r->pdid] = _ecpos_add_table($table, $table['row1'], $table['row2']);      
      }
      // Aggregate the table totals in thier table indexes
      $tables[0]['rows'][$r->year][$r->month+1]         += $r->cost;
      $tables[$r->pdid]['rows'][$r->year][$r->month+1]  += $r->cost;
    }
    }
    // Use the [ecreports] format_table function to title and format our tables
    foreach($tables as $key => $val) {
      // Use the aggregate row function to tally YTD totals, the second argument is month = monthstop - 2
      _ecpos_aggregate_row($tables[$key]['rows'], 3);
      $tables[$key]['rows'] = ecpos_format_table($tables[$key]['rows'], 0);
      $title                = $tables[$key]['title'];
      $markup .= "<h6>{$title}</h6>";
      $markup .= theme('table', array(
        'header'      => $header, 
        'rows'        => $tables[$key]['rows'],
        'attributes'  => array(
          'class' => array('ecreports', ),
        )
      ));
    }
   }
   // Provide graceful non-results
   else {
    $markup .= '<h2>There are not records for Territory #' . $tid . '</h2>';
   }
  // Return page markup
  return $markup;
}

/**
 * Function to gather correct pos data and apply emuge exceptions
 *
 * @param int $tid
 *  The territory id to use for searching
 *
 * @return obj $results 
 *  Results object containing POS sales data for reporting
 */
function _ecpos_get_pos_sales($tid) {
  // Territory IDs that need special handling
  $exceptions =  array(63, 62, 60, 92, 93, 98, 100, 601, 33, 94, 95, 96);
  // Define that tables needed to gather data
  $posTable   = 'emugecom_pos_transactions';
  $distTable  = 'emugecom_pos_distributor_info'; 
  // Build the posQuery and add the univeral conditions, joins, and columns
  $posQuery = db_select($posTable, 'pos')
    ->fields('pos', array('zip', 'cost', 'month', 'year','tid','sid','pdid'));
  $posQuery->join($distTable, 'dt', 'pos.pdid = dt.did');
  $posQuery->fields('dt', array('name'));
  $posQuery->condition('year', array('2015', '2016'), 'IN');
  $posQuery->orderBy('dt.weight');
  
  // Determine workflow based on exeption array
  if(in_array($tid, $exceptions)) {
    $posQuery->condition('pos.tid', $tid, '=');
  }
  // Normal workflow if the $tid is not exceptional. The $tid needs to 
  // be converted to the current zip mapping and the expception $tids need
  // to be excluded from the results
  else {
    $zipTable = 'emugecom_zip_state_territory_map';
    $zips     = array();
    // Build and execute the zip table query
    $zipQuery = db_select($zipTable, 'zt')
    ->fields('zt', array('zip'))
    ->condition('tid', $tid);
    try {
      $zipResults = $zipQuery->execute()->fetchAll();
    } catch(PDOException $e) {
      watchdog('ecpos', $e->getMessage(), WATCHDOG_ERROR);
      $zipResults = false;
    }
    
    // If there are zip codes, search the transaction table with them
    // Create the $zips array to use in our query
    if($zipResults != false) {
      foreach($zipResults as $key => $val) {
        array_push($zips, $val->zip);
      }
    }
    // Add the not exceptional conditions
    $posQuery->condition('zip', $zips, 'IN');
    $posQuery->condition('tid', $exceptions, 'NOT IN');
  }
  // Excute the final $posQuery query and return results
  try {
    $records = $posQuery->execute()->fetchAll();
  } catch(PDOException $e) {
    watchdog('ecpos', $e->getMessage(), WATCHDOG_ERROR);
    $records = false;
  }
  
  return $records;
}

/**
 * Function to create the territory select form for a user
 * who has access to more than one territory's POS data
 *
 * @param obj $form
 *  Drupal generated form object
 *
 * @param obj &$form_state
 *  Drupal generated reference object
 *
 * @return obj $form
 *  Renderable form after modifications
 */
function ecpos_managerview_tid_select_form($form, &$form_state) {
  
  drupal_set_title('POS/Drop Ship Reporting - Manager View - Territory Select');
  
  $form['tid'] = array(
    '#title' => 'Select a Territory ID',
    '#type' => 'select',
    '#options' => drupal_map_assoc($form_state['build_info']['args'][0]),
  );
  
  $form['action']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Select TID',
  );
  
  return $form;
}

/**
 * Submit form handler for the tid select form
 *
 * @param obj $form
 *  Drupal provided $form object
 *
 * @param obj &$form_state
 *  Drupal provided $form_state object
 */
function ecpos_managerview_tid_select_form_submit($form, &$form_state) {
  drupal_goto('/pos/managerview', array('query' => array('tid' => $form_state['values']['tid'])));
}

/**
 * Function to determine whether a user should be able to view a given territory id
 * sales information
 *
 * @retrun bool
 *  Boolean value to determine whether the user should be granted permission to view the page
 */
function _ecpos_managerview_check_access() {
  // Load the current user
  global $user;
  // set the default access value
  $access = true;
  if(count($user->roles) <= 1) {
    $access = false;
  };

  $tid = (isset($_GET['tid'])) ? $_GET['tid'] : null;
  // Check access for territory manager
  if(isset($user->roles[7]) && !in_array($tid, array(0, '', null))) {
    // Load the full user to get fields
    $user = user_load($user->uid);
    // Initialize and populate the allowed $tms array
    $tms = array();
    foreach($user->field_emuge_tmn['und'] as $key => $val) {
      array_unshift($tms, $val['value']);
    }
    // Deny access if $tid is not in the allowed $tms array
    if(!in_array($tid, $tms)) {
      $access = false;
    }
  }
  // check access for admin, staff, and accounting with a tid
  elseif(!isset($user->roles[3]) && isset($user->roles[6]) && !isset($user->roles[11]) && !in_array($tid, array(0, '', null))) {
    $access = false;
  }
  
  // return the $access value for processing
  return $access;
}
