<?php

/**
 * Implements hook_views_data().
 */
function emuge_pos_views_data() {
    
    $data = array();
    $pos = 'emugecom_pos_transactions';
    $tms = 'emugecom_territory_managers';
    $dis = 'emugecom_pos_distributor_info';
    $sts = 'emugecom_states';
    
    $data[$pos]['table']['group'] = t('POS Transactions');

    $data[$pos]['table']['base'] = array(
        'title' => t('POS Records'),
        'help' => t('Contains POS records we want exposed to Views.'),
    );
    
   
    
    $data[$pos]['pid'] = array(
        'title' => t('ID'),
        'help' => t('The record ID.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
    );
    
     $data[$pos]['pdid'] = array(
        'title' => t('Distributor ID'),
        'help' => t('POS record distributor number.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
    );
    
    $data[$pos]['tid'] = array(
        'title' => t('Territory ID'),
        'help' => t('The record Territory ID.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
    );
    
    $data[$pos]['sid'] = array(
        'title' => t('State ID'),
        'help' => t('The record State ID.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
    );
    
    $data[$pos]['puid'] = array(
        'title' => t('User ID'),
        'help' => t('The record Territory Manager\'s User ID.'),
        'field' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
    );
    
    $data[$pos]['city'] = array(
        'title' => t('Distributor City'),
        'help' => t('The record Distributor city.'),
        'field' => array(
            'handler' => 'views_handler_field',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    $data[$pos]['cost'] = array(
        'title' => t('Amount'),
        'help' => t('The record dollar amount.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
    );
    
    $data[$pos]['zip'] = array(
        'title' => t('Zip'),
        'help' => t('The record zip code.'),
        'field' => array(
            'handler' => 'views_handler_field',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    $data[$pos]['month'] = array(
        'title' => t('Month'),
        'help' => t('The record posting month.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
    );
    
    $data[$pos]['year'] = array(
        'title' => t('Year'),
        'help' => t('The record posting year.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
    );
    
    //tm table
    $data[$tms]['table']['group'] = t('TMs Info');
    $data[$tms]['table']['base'] = array(
            'title' => t('TMs Records'),
            'help' => t('Contains POS records we want exposed to Views.'),
    );  
    
    $data[$tms]['uid'] = array(
        'title' => t('Unique User ID'),
        'help' => t('The Unique User ID..'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
    );
    
    $data[$tms]['first'] = array(
        'title' => t('TMs First Name'),
        'help' => t('The TMs First Name.'),
        'field' => array(
            'handler' => 'views_handler_field',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    $data[$tms]['last'] = array(
        'title' => t('TMs Last Name'),
        'help' => t('The TMs Frist Name.'),
        'field' => array(
            'handler' => 'views_handler_field',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    $data[$tms]['title'] = array(
        'title' => t('TMs Title '),
        'help' => t('The TMs Title Name.'),
        'field' => array(
            'handler' => 'views_handler_field',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    $data[$tms]['phone'] = array(
        'title' => t('TMs Phone '),
        'help' => t('The TMs phone number.'),
        'field' => array(
            'handler' => 'views_handler_field',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    $data[$tms]['email'] = array(
        'title' => t('TMs Eamil '),
        'help' => t('The TMs email address.'),
        'field' => array(
            'handler' => 'views_handler_field',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    $data[$dis]['table']['group'] = t('POS Distributors');

    $data[$dis]['table']['base'] = array(
        'title' => t('POS Distriburtors'),
        'help' => t('Contains POS Distriburtors we want exposed to Views.'),
    );
    
    $data[$dis]['did'] = array(
        'title' => t('Unique Record ID'),
        'help' => t('The Unique record ID.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    $data[$dis]['name'] = array(
        'title' => t('POS Distriburtor Name'),
        'help' => t('The POS Distriburtor Name.'),
        'field' => array(
            'handler' => 'views_handler_field',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    $data[$sts]['table']['group'] = t('US/CA States');

    $data[$sts]['table']['base'] = array(
        'title' => t('Emuge States Table'),
        'help' => t('Contains data about states.'),
    );
    
    $data[$sts]['sid'] = array(
        'title' => t('The Unique State ID'),
        'help' => t('The State ID.'),
        'field' => array(
            'handler' => 'views_handler_field',
         ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
            'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
   $data[$sts]['ST'] = array(
        'title' => t('State Abbreviation'),
        'help' => t('The State Abbreviation.'),
        'field' => array(
            'handler' => 'views_handler_field',
         ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
            'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    $data[$sts]['name'] = array(
        'title' => t('Full State Name'),
        'help' => t('The State Name.'),
        'field' => array(
            'handler' => 'views_handler_field',
         ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
            'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
    );
    
    
    
      // table join
    $data[$tms]['table']['join'] = array(
        $pos => array(
            'left_field' => 'puid',
            'field' => 'uid',
        ),
    );
    
    $data[$dis]['table']['join'] = array(
        $pos => array(
            'left_field' => 'pdid',
            'field' => 'did',
        ),
    );
    
    $data[$sts]['table']['join'] = array(
        $pos => array(
            'left_field' => 'sid',
            'field' => 'sid',
        ),
    );
    
    return $data;

}