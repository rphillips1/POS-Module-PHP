<?php
/**
 * Function to display the accounting view pos search form
 *
 * @param obj $form
 *  Drupal provided $form object
 *
 * @param obj &$form_state\
 *  Drupal provided $form_state reference object
 */
function ecpos_accounting_form_page($form, &$form_state) {
    
    drupal_add_css(drupal_get_path('module', 'ecpos') . '/css/ecpos_styles.css');
 
    $form['year'] = array(
        '#type' => 'select',
        '#title' => t('Year'),
        '#options' => array(
            2013 => '2013',
            2014 => '2014',
            2015 => '2015',
            2016 => '2016',
        ),
        '#default_value' => '2015'
    );
    $form['month'] = array(
        '#type' => 'select',
        '#title' => t('Month'),
        '#options' => _init_ecpos_form_options('months'),
        '#default_value' => '0'
    );
    
    $form['pdid'] = array(
        '#type' => 'select',
        '#title' => t('Distributor'),
        '#options' => _init_ecpos_form_options('distributors'),
        '#default_value' => 0
    );
    
    $form['puid'] = array(
        '#type' => 'select',
        '#title' => t('Manager'),
        '#options' => _init_ecpos_form_options('managers'),
        '#default_value' => 0
    );
    
    $form['tid'] = array(
        '#type' => 'select',
        '#title' => t('Territory'),
        '#options' => _init_ecpos_form_options('territory'),
        '#default_value' => 0
    );
    
    $form['apply'] = array(
        '#type' => 'submit',
        '#value' => t('Apply'),
        '#ajax' => array(
            'callback' => '_handle_ecpos_search_form_update',
            'wrapper' => 'accounting-form-result',
            'method' => 'replace'
        ),
        '#weight' => 90,
    );
    
    $form['allRecords'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show Records'),
        '#weight' => 95,
    );
    
    $form['historical'] = array(
        '#type' => 'hidden',
        '#title' => t('Historical Territory Search'),
        
    );

    $form['feedback'] = array(
        '#type' => 'hidden',
        '#markup' => '<a href="/content/emuge-tpr-v14x-feedback" onclick="window.open(this.href, \'\', \'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no,width=500,height=800\'); return false;">Feedback</a>',
    );
    
    $form['result'] = array(
        '#type' => 'markup',
        '#markup' => '',
        '#prefix' => '<div id="accounting-form-result">',
        '#suffix' => '</div>',
        '#weight' => 101,
    );
    
    return $form;
}

/**
 * Function that performs the searching, organizing, and aggregation of
 * the different filters applied to it
 *
 * @param object $form           The form object passed during the AJAX call
 * @param object $form_state     The form_state object passed during the AJAX call
 *
 * @return array $results        The sorted array of results
 */
function _handle_ecpos_search_form_update($form, $form_state){
  $results = _ecpos_query_pos($form, $form_state);
  $markup = _ecpos_get_markup($form, $form_state, $results);
  $form['result']['#weight'] = 500;
  $form['result']['#markup'] = $markup;
  return $form['result'];
}
