<?php
/**
 * @file
 *  Contains the views,forms, and code for displaying historical, comparative, and differential
 *  data about an Emuge territory
 */

/**
 * Display the record differences between the manager and accounting view
 * of the pos app
 *
 * @param string $tid
 *  The terriorry id to report
 *
 * @param string $month
 *  Additional conditional information for forming the query
 *
 * @param string $year
 *  Additional conditional information for forming the query
 */
function ecpos_historicalview_view($tid = null, $month = null, $year = 2015) {
  drupal_add_css(drupal_get_path('module', 'ecpos') . '/css/ecpos_styles.css');
  $cid = "ecpos_historicalview_{$tid}_chache";
  // If a cached entry exists, return it
  if ($cached = cache_get($cid)) {
    return $cached->data;
  }
  
  if(!_ecpos_pos_exceptions('tid', $tid)) {
    $data   = array();
    $types  = array('tid', 'zip', 'diffs');
    $titles = array(
      'tid'   => t('Accounting View'),
      'zip'   => t('Manager View'),
      'diffs' => t('Difference View'),
    );
    $tableRows = array();
    
    foreach($types as $type) {
      $data[$type] = _ecpos_get_historicalview_proto('historical');
      
      $data[$type]['title'] = $titles[$type];
      $data[$type]['table'] = _ecpos_get_pos_data($tid, $type, $data[$type]['table']);
    }
    
    $data['tid']['diffs']['recs'] = array_diff($data['tid']['table']['recs'], $data['zip']['table']['recs']);
    $data['zip']['diffs']['recs'] = array_diff($data['zip']['table']['recs'], $data['tid']['table']['recs']);
    
    $data['tid']['table'] = _ecpos_get_pos_data( $data['tid']['diffs']['recs'], 'tdiff', $data['tid']['table']);
    $data['zip']['table'] = _ecpos_get_pos_data( $data['zip']['diffs']['recs'], 'zdiff', $data['zip']['table']);
    $tableRows = _ecpos_total_historicalview_rows($data['tid']['table']['rows'][0], $data['zip']['table']['rows'][0], 'ACCT-MGR', false);
    $diffRows = _ecpos_total_historicalview_rows($data['tid']['table']['rows'][1], $data['zip']['table']['rows'][1], 'TID D-Zips D', false);
    
    foreach($diffRows as $dr) {
      $tableRows[] = $dr;
    }
    foreach($tableRows as $key => $rs) {
      foreach($rs as $ckey => $cell) {
        if($ckey != 0) {
          $number = in_array($rs[0], array('TID-Zips', 'TID D-Zips D', 'ACCT-MGR')) ? 
            '<span style="font-weight:bold;">' . number_format($tableRows[$key][$ckey]) . '</span>' :
            number_format($tableRows[$key][$ckey]);
          $tableRows[$key][$ckey] = $number;
        }
      }
    }
    $markup = '<h1>Historical View for Territory ' . l($tid, '/pos/historicalview/' . $tid) . '</h1>';
    $markup .= theme('table', array('header' => $data['tid']['table']['header'], 'rows' => $tableRows));
  }
  else {
    $markup = 'This Territory is on the exception list';
  }
  cache_set($cid, $markup);
  return $markup;
}

function _ecpos_get_historicalview_proto($type = 'historical') {
  switch($type) {
    case 'historical' :
      $return =  array(
        'title' => '',
        'table' => array(
          'title'   => '',
          'header'  => array(),
          'rows'    => array(),
          'recs'    => array(),
        ),
        #'total' => 0,
        'diffs' => array(
          'recs'  => array(),
          'count' => 0,
        ),
      );
    break;
    default : 
  }
  
  return $return;
}

/**
 * Display the results to the user
 *
 * @param array $data
 *  The data to render in the tables
 *
 * @param string $type
 *  The type of display to render
 */
function ecpos_historicalview_display_results_page($data = null, $type = null){
  drupal_add_css(drupal_get_path('module', 'ecpos') . '/css/ecpos_styles.css');
  $cid = 'ecpos_historicalview_chache';
  // If a cached entry exists, return it
  if ($cached = cache_get($cid)) {
    return $cached->data;
  }
  
  $posTable = 'emugecom_pos_transactions';
  $markup = '';
  $query = db_select($posTable, 'pos')->fields('pos', array('tid')); 
  $query->condition('tid', _ecpos_pos_exceptions('tid'), 'NOT IN');
  #$query->distinct();
  try{
    pc(_ecpos_pos_exceptions('tid'), '_ecpos_pos_exceptions()');
    $tidResults = array_keys($query->execute()->fetchAllKeyed(0, 0));
  } catch(PDOException $e) {
    watchdog('ecpos', $e->getMessage(), WATCHDOG_ERROR);
  }
  
  if($tidResults) {
    foreach($tidResults as $tid) {
      $markup .= ecpos_historicalview_view($tid);
    }
  } else {
    $markup = 'Something went wrong.';
  }
  cache_set($cid, $markup);
  return $markup;
}

/**
 * Get the pos table results, records, or other specific search types
 *
 * @param string $tid
 *  The territory id to search
 *
 * @param string $type
 *  The type of query to perform
 *
 * @return mixed $results
 *  The results of the query
 */
function _ecpos_get_pos_data($tid, $type, $table, $year = 2015, $month = null) {
  $posTable   = 'emugecom_pos_transactions';
  $data = array();
  
  switch($type){
    case 'tid':
      $query = db_select($posTable, 'pos')->fields('pos');
      $query->condition('year', $year);
      $query->condition('tid', $tid);
      
      if(!is_null($month)) {
        $query->condition('month', $month);
      }
      
      try{
        $results = $query->execute()->fetchAll();
      }
      catch(PDOexception $e) {
        watchdog('ecpos', $e->getMessage(), WATCHDOG_ERROR);
      }
      
      if($results) {
        $table['title'] = 'Accounting View Totals for Territory #' . $tid;
        $table['header'] = array(
          '', 
          l('Total', "pos/testview-year/{$tid}/{$year}"), 
          l('Jan', "pos/testview/{$tid}/1/{$year}"), 
          l('Feb', "pos/testview/{$tid}/2/{$year}"), 
          l('Mar', "pos/testview/{$tid}/3/{$year}"), 
          l('Apr', "pos/testview/{$tid}/4/{$year}"),
          l('May', "pos/testview/{$tid}/5/{$year}"), 
          l('Jun', "pos/testview/{$tid}/6/{$year}"), 
          l('Jul', "pos/testview/{$tid}/7/{$year}"), 
          l('Aug', "pos/testview/{$tid}/8/{$year}"),
          l('Sep', "pos/testview/{$tid}/9/{$year}"),
          l('Oct', "pos/testview/{$tid}/10/{$year}"),
          l('Nov', "pos/testview/{$tid}/11/{$year}"),
          l('Dec', "pos/testview/{$tid}/12/{$year}"),
        );
        $row = array();
        $recs = array();
        $row = array_pad(array(), 14,0);
        foreach($results as $r) {
          $row[0] = ($row[0] == 0) ? 'ACCT View': $row[0];
          $row[1] += $r->cost;
          $row[$r->month + 1] += $r->cost;
          $table['recs'][] = $r->pid;
        }
        $table['rows'][] = $row;
      }
    break;
    
    case 'zip':
      $zipTable   = 'emugecom_zip_state_territory_map';
      $zipMapQuery = db_select($zipTable, 'zip')->fields('zip', array('zip'));
      $zipMapQuery->condition('tid', $tid);
      try{
        $zipMapResults = array_keys($zipMapQuery->execute()->fetchAllKeyed(0, 0));
      }
      catch(PDOexception $e) {
        watchdog('ecpos', $e->getMessage(), WATCHDOG_ERROR);
      }
      
      $zipQuery = db_select($posTable, 'pos')->fields('pos');
      $zipQuery->condition('year', $year);
      $zipQuery->condition('zip', $zipMapResults, 'IN');
      $zipQuery->condition('tid', _ecpos_pos_exceptions('tid'), 'NOT IN');
      
      if(!is_null($month)) {
        $zipQuery->condition('month', $month);
      }
      
      try{
        $zipQueryResults = $zipQuery->execute()->fetchAll();
      }
      catch(PDOexception $e) {
        watchdog('ecpos', $e->getMessage(), WATCHDOG_ERROR);
      }
      
      if($zipQueryResults) {
        $table['title'] = 'Manager View Totals for Territory #' . $tid;
        $table['header'] = array('', 'Total', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        $row = array();
        $recs = array();
        $row = array_pad(array(), 14,0);
        foreach($zipQueryResults as $r) {
          $row[0] = ($row[0] == 0) ? 'MGR View' : $row[0];
          $row[1] += $r->cost;
          $row[$r->month + 1] += $r->cost;
          $table['recs'][] = $r->pid;
        }
        $table['rows'][] = $row;
      }
    break;
    
    case 'tdiff':
      $query = db_select($posTable, 'pos')->fields('pos');
      $query->condition('pid', $tid, 'IN');
      
      try{
        $results = $query->execute()->fetchAll();
      }
      catch(PDOexception $e) {
        watchdog('ecpos', $e->getMessage(), WATCHDOG_ERROR);
      }
      if($results) {
       $row = array();
        $row = array_pad(array(), 14,0);
        foreach($results as $r) {
          $row[0] = ($row[0] == 0) ? 'TIDs not in Zip' : $row[0];
          $row[1] += $r->cost;
          $row[$r->month + 1] += $r->cost;
        }
      }
      
      if($row) {
        $table['rows'][] = $row;
      }
      
    break;
     case 'zdiff':
      $query = db_select($posTable, 'pos')->fields('pos');
      $query->condition('pid', $tid, 'IN');
      
      try{
        $results = $query->execute()->fetchAll();
      }
      catch(PDOexception $e) {
        watchdog('ecpos', $e->getMessage(), WATCHDOG_ERROR);
      }
      if($results) {
       $row = array();
        $row = array_pad(array(), 14,0);
        foreach($results as $r) {
          $row[0] = ($row[0] == 0) ? 'Zips not in TIDs' : $row[0];
          $row[1] += $r->cost;
          $row[$r->month + 1] += $r->cost;
        }
      }
      
      if($row) {
        $table['rows'][] = $row;
      }
      
    break;
    default:  
  }
  pc($table, '$table');
  return $table;
}

function _ecpos_total_historicalview_rows($rows1, $rows2, $titleCell = null, $add = true) {
  
  $rows = array();
  $row = array();
  $row = array_pad(array(), 13, 0);
  $row[0] = is_null($titleCell) ? '' : $titleCell;
  pc($rows1, '$rows1');
  pc($rows2, '$rows2'); 
  for($i = 1; $i <= 13; $i++) {
   
    if($add) {
      $row[$i] = $rows1[$i] + $rows2[$i];
    } 
    else {
      $row[$i] = $rows1[$i] - $rows2[$i];
    }
    
    pc($i, '$i');
  }
 
  $rows[] = $rows1;
  $rows[] = $rows2;
  $rows[] = $row;
  
  return $rows;
}

/**
 * Get the pos table results, records, or other specific search types
 *
 * @param string $tids
 *  The tid records to compare
 *
 * @param string $zips
 *  The zip records to compare
 *
 * @return array $results
 *  The results of the comparison
 */
function _ecpos_historicalview_get_differences($tids, $zips) {

}

/**
 * Add a row to the historical view array
 *
 * @param string $title
 *  Title of the row for cell 0
 *
 * @param int $cells
 *  Number of cells to apply to the row
 *
 * @return array $row
 *  Row to be added to a $rows variable
 */
function _ecpos_historicalview_add_row($title, $cells = 13){
}

/**
 * Add a row to the historical view array
 *
 * @param array $rows
 *  array of rows to have totaled
 *
 * @return array $rows
 *  Updated rows array with the total row added
 */
function _ecpos_historicalview_add_totals_row($rows){
}

######### Functions for tid select form and submit handler ################
/**
 * Display the historical view criteria select form
 *
 * @param obj $form
 *  Drupal provided form object
 * 
 * @param obj &$form_state
 *  Drupal provided form state reference object
 *
 * @return obj $form
 *  Updated Drupal object to be rendered 
 */
function ecpos_historicalview_select_form($form, &$form_state) {
  pc($form_state, 'fs');
  drupal_add_css(drupal_get_path('module', 'ecpos') . '/css/ecpos_styles.css');
  $exceptions       =  array(63, 62, 60, 92, 93, 98, 100, 601, 33, 94, 95, 96);
  // Remove the exceptions from the selectable display
  $tidOptions       = array_diff(_init_ecpos_form_options('territory'), $exceptions);
  $monthOptions     = _init_ecpos_form_options('months');
  $monthOptions[0]  = 'All';
  // Remove the default options from the selectable display
  unset($tidOptions[0]);
  
  
  $form['tid'] = array(
    '#type' => 'select',
    '#title' => t('Territory'),
    '#options' => $tidOptions,
    '#default_value' => isset($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : 1,
  );
  
  $form['year'] = array(
    '#type' => 'select',
    '#title' => t('Year'),
    '#options' => array(
      2014 => '2014',
      2015 => '2015',
      2016 => '2016',
    ),
    '#default_value' => isset($form_state['build_info']['args'][2]) ? $form_state['build_info']['args'][2] : 2015,
  );
    
  $form['month'] = array(
    '#type' => 'select',
    '#title' => t('Month'),
    '#options' => $monthOptions,
    '#default_value' => isset($form_state['build_info']['args'][1]) ? $form_state['build_info']['args'][1] : 0,
  );
  
  $form['action']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'View Report',
  );
    
  return $form;
}

/**
 * Handle the ecpos_historicalview_select_form form submit action. Redirect
 * to the appropriate url
 *
 * @param obj $form
 *  Drupal provided form object
 * 
 * @param obj &$form_state
 *  Drupal provided form state reference object
 *
 */
function ecpos_historicalview_select_form_submit($form, &$form_state) {
  $tid    = $form_state['values']['tid'];
  $month  = $form_state['values']['month'];
  $year   = $form_state['values']['year'];
  // If the month value is 0, the user would like to see the entire years report
  $path = ($month > 0) ? "/pos/historicalview/{$tid}/{$month}/{$year}" : "/pos/historicalview/year/{$tid}/{$year}";
  drupal_goto($path);
}

function __ecpos_get_abstract(){
  
  $Table_ = array(
    'title'   => $table_title,
    'header'  => $tableHeader,
    'rows'    => array(
        $tid      => array($tid, $total, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
        $tid      => array($tid, '...'),
        'totals'  => array('Totals', $yearTotal, $janTotal, $febTotal, '...'),
    ),
  ); // End of $tidTable array
  
  $data = array(
    'tid' => array(
      'title' => $tidTitle,
      'table' => $Table_,
      'total' => $tidTotal,
      'diffs' => array(
        'recs'  => array(),
        'count' => count($tids),
      ),
    ),
    
    'zip' => array(
      'title' => $zipTitle,
      'table' => $Table_,
      'total' => $zipTotal,
      'diffs' => array(
        'recs'  => array($recs),
        'count' => count($zips),
      ),
    ),
    
    'diffs' => array(
      'title' => $diffsTitle,
      'table' => array(
        'header'  => $diffHeader,
        'rows'    => array(
          'acct'    => array('Accounting', $acctTotal, $acctJan, '...'),
          'mgr'     => array('Manager', $mgrTotal, $mgrJan, '...'),
          'totals'  => array('Acct - Mgr', $diffTotal, $diffJan, '...'),
        ),
      ),
      
    ),
  ); // End of $data array
}