<?php

/**
 * @file
 *  Contains the code to test both the accounting and manager views of the emuge pos app
 */

/**
 * Display the testview criteria select form
 *
 * @param obj $form
 *  Drupal provided form object
 * 
 * @param obj &$form_state
 *  Drupal provided form state reference object
 *
 * @return obj $form
 *  Updated Drupal object to be rendered 
 */
function ecpos_testview_select_form($form, &$form_state) {
  
  drupal_add_css(drupal_get_path('module', 'ecpos') . '/css/ecpos_styles.css');
  $exceptions       =  array(63, 62, 60, 92, 93, 98, 100, 601, 33, 94, 95, 96);
  // Remove the exceptions from the selectable display
  $tidOptions       = array_diff(_init_ecpos_form_options('territory'), $exceptions);
  $monthOptions     = _init_ecpos_form_options('months');
  $monthOptions[0]  = 'All';
  // Remove the default options from the selectable display
  unset($tidOptions[0]);
  
  $form['tid'] = array(
    '#type' => 'select',
    '#title' => t('Territory'),
    '#options' => $tidOptions,
    '#default_value' => isset($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : 1,
  );
  
  $form['month'] = array(
    '#type' => 'select',
    '#title' => t('Month'),
    '#options' => $monthOptions,
    '#default_value' => isset($form_state['build_info']['args'][1]) ? $form_state['build_info']['args'][1] : 0,
  );
  
  $form['year'] = array(
    '#type' => 'select',
    '#title' => t('Year'),
    '#options' => array(
      2014 => '2014',
      2015 => '2015',
      2016 => '2016',
    ),
    '#default_value' => isset($form_state['build_info']['args'][2]) ? $form_state['build_info']['args'][2] : 2015,
  );
  
  $form['action']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'View Report',
  );

  return $form;
}

/**
 * Handle the ecpos_testview_select_form form submit action. Redirect
 * to the appropriate url
 *
 * @param obj $form
 *  Drupal provided form object
 * 
 * @param obj &$form_state
 *  Drupal provided form state reference object
 *
 */
function ecpos_testview_select_form_submit($form, &$form_state) {
  $tid    = $form_state['values']['tid'];
  $month  = $form_state['values']['month'];
  $year   = $form_state['values']['year'];
  // If the month value is 0, the user would like to see the entire years report
  $path = ($month > 0) ? "/pos/testview/{$tid}/{$month}/{$year}" : "/pos/testview-year/{$tid}/{$year}";
  drupal_goto($path);
}
 
 /**
  * Function to test the the different workflows of the emuge pos app
  *
  * @param string $tid
  *   The territory to test
  *
  * @param int $month
  *   Month integer default of 12
  *
  * @param int $year
  *   Year integer default of 2015
  *
  * @return obj $page
  *   Drupal renderable $page object
  */
function ecpos_testview_view($tid, $month = 12, $year = 2015, $form = true) {
  $cid = "ecpos_testview_view_{$tid}_{$month}_{$year}_cache";
  // If a cached entry exists, load the markup, add applicable select form, and return entry
  if ($cached = cache_get($cid)) {
    $markup =  $cached->data;
    if($form == true) {
      $tidForm = drupal_get_form('ecpos_testview_select_form', $tid, $month, $year);
      $markup = drupal_render($tidForm) . $markup;
    }
    return $markup;
  }
   
  $page = array();
  $posTable   = 'emugecom_pos_transactions';
  $zipTable   = 'emugecom_zip_state_territory_map';
  $distTable  = 'emugecom_pos_distributor_info';
  $tmtable    = 'emugecom_territory_managers';
  $tablesHTML = '';
  $markup     = '';
  $diffCount  = 0;
  $tableTotal = 0;
  $NonDSTotal = 0;
  $yearPath   = '/pos/testview-year/' . $tid . '/' . $year;
  $monthPath  = '/pos/testview/' .$tid . '/' . $month . '/' . $year;
  $date       = '<a href="' . $monthPath . '" title="Click to only see this months test for Territory #' . $tid . '">' . $month . 
                '</a>/<a href="' . $yearPath . '" title="Click to see all of ' . $year . ' tests for Territory #' . $tid . '">' . $year . '</a>';
  // Array of territory id exceptions
  $exceptions =  array(63, 62, 60, 92, 93, 98, 100, 601, 33, 94, 95, 96);
  // This was used for prototyping, remove
  $query = db_select($posTable, 'pos')->fields('pos', array('pid'));
  $query->condition('month', $month);
  $query->condition('year', $year);  
  // Process the $tid if it is not in the list of exceptions
  if(!in_array($tid, $exceptions)) {
    // First, mimic the accounting view search via the territory id
    $diffsArray = array();
    $tidQuery = db_select($posTable, 'pos')->fields('pos', array('pid'));
    $tidQuery->condition('month', $month);
    $tidQuery->condition('year', $year);
    $tidQuery->condition('tid', $tid);
    
    $tidResults = array_keys($tidQuery->execute()->fetchAllKeyed(0, 0));
    
    // Second, mimic the manager view search via a zip code conversion resulting $zipMapResults
    $zipMapQuery = db_select($zipTable, 'zip')->fields('zip', array('zip'));
    $zipMapQuery->condition('tid', $tid);
    
    $zipMapResults = array_keys($zipMapQuery->execute()->fetchAllKeyed(0, 0));
    
    // Third, Use $zipMapResults against the same POS table
    $zipQuery = db_select($posTable, 'pos')->fields('pos', array('pid'));
    $zipQuery->condition('month', $month);
    $zipQuery->condition('year', $year);
    // Add the $zipMapResults condition
    $zipQuery->condition('zip', $zipMapResults, 'IN');
    // Per the managerview, mimic the expecptions exclusion from the search
    $zipQuery->condition('tid', $exceptions, 'NOT IN');
    
    $zipResults = array_keys($zipQuery->execute()->fetchAllKeyed(0, 0));
    
    ############ Differential Valuation ############### 
    // The use of separate indexes allows us to differentiate the results source
    $diffsArray[] = array_diff($tidResults, $zipResults);
    $diffsArray[] = array_diff($zipResults, $tidResults);
    
    // Process $diffsArray from the TID <> ZIP results
    foreach($diffsArray as $diffs) {
      // The title variable toggles the table titles
      $title = !isset($title) ? count($diffs) . ' Values in TID results not present in Zip results - ' : 
                                count($diffs) . ' Values in Zip results not present in TID results - ' ;
      
      // Count the total differences and find the records for display
      if(isset($diffs) && count($diffs) > 0) {
        $diffCount += count($diffs);
        $diffsQuery = db_select($posTable, 'dif')->fields('dif');
        $diffsQuery->join($tmtable, 'tm', 'dif.puid = tm.uid');
        $diffsQuery->join($distTable, 'dis', 'dif.pdid = dis.did');
        $diffsQuery->addExpression("CONCAT(tm.first, ' ', tm.last)", 'tm_name');
        $diffsQuery->addExpression("dis.name", 'distributor');
        $diffsQuery->condition('pid', (array)$diffs, 'IN');
        
        $diffsResults = $diffsQuery->execute()->fetchAll();
        
        // Build the theme table variables with the results
        foreach($diffsResults as $records) {
          if(!isset($header)) {
            $header = array_keys((array)$records);
          }
          $tableTotal += $records->cost;
          if($records->distributor != 'DropShips') {
            $NonDSTotal += $records->cost;
          }
          $rows[] = array_values((array)$records);
        }
        $tableTotal = number_format($tableTotal, 2);
        $title .= ' <span style="font-style:italic;">Total: $' . $tableTotal . '</span>';
        if($NonDSTotal > 0) {
          $title .= ' <span style="color:red; font-size:medium; font-style:italic;">(Non DropShips Total: $' . number_format($NonDSTotal, 2) . ')</span>';
        }
        // Append the table results html to $tablesHTML and reset $rows for the next iteration
        $tablesHTML   .= "<h2>$title</h2>";                          
        $tablesHTML   .= theme('table', array('header' => $header, 'rows' => $rows));
        $rows         = array();
        $tableTotal   = 0;
        $NonDSTotal   = 0;
      }
    }
    // Add the table title markup
    $markup .= '<h2 style="color:blue;">Territory #' . $tid . ' POS Test for the month of ' . $date . '</h2>';
    // Handle not finding any TID results
    if(count($tidResults) == 0) {
      $markup .= "<span style=\"color:orange;\">NO TID RESULTS</span> - There are no records for territory #{$tid} in the POS table for {$date}";
    }
    if(count($zipResults) == 0) {
      $markup .= "<span style=\"color:orange;\">NO ZIP RESULTS</span> - There are no records for territory #{$tid} in the POS table for {$date}";
    }
    // Handle not finding any differences between TID <> ZIP results
    if($diffCount == 0) {
      $markup .= '<p><span style="color:#00FF00;">NO DISCREPANCIES</span> - Both the Accouting view and the Manager view have ' . 
                  count($tidResults).' Records with 0 differences.</p>';
    }
    // Hanlde displaying differences found between TID <> ZIP results
    else {
      #$d = count($tidResults) - count($zipResults);
      $countText = $diffCount == 1 ? 'is 1 discrepancy': 'are ' . $diffCount . ' discrepancies';
      $markup .= '<p><span style="color:red;">DISCREPANCIES </span> - There ' . $countText . ' between the views<p>';
      $markup .= '<p>Accounting View has ' . count($tidResults) . ' Records, ';
      $markup .= 'Manager View has ' . count($zipResults) . ' Records, '; 
      $markup .= 'Territory #' . $tid . ' has ' . count($zipMapResults) . ' Zips associated with it.</p>';
      $markup .= $tablesHTML;
      #$markup .= '<em><span style="font-weight:bold;">http://www.emuge.co/pos/testview/territory_id</span>/[month]/[year] (month and year are optional, default is 12/2015)</em><br /><br />';
    }
  }
  // Handle finding the TID on the exceptions list
  else {
    $markup = "<span style=\"color:red;\">EXCEPTION</span> - Territory $tid is an Exception Territory and does not have a list
                of zip codes.";
  }
  // Cache $markup values
  cache_set($cid, $markup);
    
  if($form == true) {
    $tidForm = drupal_get_form('ecpos_testview_select_form', $tid, $month, $year);
    $markup = drupal_render($tidForm) . $markup;
  }
  
  return $page['markup'] = $markup;
 }

/**
 * Function to process the ecpos_testview_view function once for each month of a year
 *
 * @param int $tid
 *  The territory id to process
 *
 * @param int $year
 *  The year to build the test views in
 *
 * @return array $page
 *  A markup array suited for drupal rendering
 */
function ecpos_testview_view_year($tid, $year) {
  // Array of territory id exceptions. This is checked to prevent 12 exception notices to display
  $exceptions = array(63, 62, 60, 92, 93, 98, 100, 601, 33, 94, 95, 96);
  $page       = array();
  // Process the $tid if it is not in the list of exceptions
  if(!in_array($tid, $exceptions)) {
    // Fire the function once for each month
    for($i = 1; $i <= 12; $i++) {
     $html =  ecpos_testview_view($tid, $i, $year, false);
     $markup .= $html;
    }
  }
  // Handle an exception $tid only once
  else {
    $markup = "<span style=\"color:red;\">EXCEPTION</span> - Territory $tid is an Exception Territory and does not have a list of zip codes.";
  }
  $tidFrom = drupal_get_form('ecpos_testview_select_form', $tid, $i, $year);
  $markup = drupal_render($tidFrom) . $markup;
  
  return $markup;
}